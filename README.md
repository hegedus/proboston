# Django app for PROBOSTON
## Description
  The application has one page with the form for saving name, email, ICO.  
  
  The ICO is validated against public API ARES [(see)](https://wwwinfo.mfcr.cz/ares/ares_xml.html.cz)\.   
  It uses *Standard* endpoint -`základní dotaz na výpis identifikačních údajů: Standard`  
  
  Model is visible in Admin-site with forbidden possibility to add the records.

## Used packages and python version
  The project was implemented using Python3.6
  
###  Packages:
  - Django==2.2
  - requests==2.24
  - beautifulsoup4==4.9.1

  The project contains `requirements.txt` file for simple installtion.

## Installation on Linux

  `virtualenv --python=python3.6 python3.6`   
  `source python3.6/bin/activate`   
  `cd proboston`     
  `./manage.py migrate`   
  `./manage.py runserver`   

from django.conf.urls import url
from contractors import views as contr_views

urlpatterns = [
    url('', contr_views.ContractorView.as_view(), name='add-contractor')
]

from django import forms
from contractors.models import Contractor
from contractors.validators import validate_ico
from django.core.validators import MinLengthValidator, MaxLengthValidator


class ContractorForm(forms.ModelForm):
    ico = forms.CharField(
        max_length=8,
        validators=[
            MinLengthValidator(8),
            MaxLengthValidator(8),
            validate_ico
        ]
    )

    class Meta:
        model = Contractor
        fields = '__all__'

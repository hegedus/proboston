from django.views.generic.edit import FormView
from contractors.forms import ContractorForm


class ContractorView(FormView):
    template_name = 'contractors/add_contractor.html'
    form_class = ContractorForm
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

import requests
from bs4 import BeautifulSoup

from django.core.exceptions import ValidationError

# See: https://wwwinfo.mfcr.cz/ares/ares_xml.html.cz
ares_url = 'https://wwwinfo.mfcr.cz/cgi-bin/ares/darv_std.cgi'


def validate_ico(ico):
    """
    This function requests the ARES to find the ICO.
    It parses the returned XML. If element <are:pocec_zaznamu> is present and
    number inside is higher than 1, the ICO was successfuly found.

    Args:
        ico (str): ICO to validate

    Raises:
        ValidationError
    """

    req_res = requests.get(ares_url, params={'ico': ico})
    soup = BeautifulSoup(req_res.text)
    records_count_search = soup.find_all('are:pocet_zaznamu')

    if (
        (not len(records_count_search))
        or (not int(records_count_search[0].get_text()))
    ):
        raise ValidationError('ICO: {} was not found in ARES.'.format(ico))

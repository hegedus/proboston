from django.contrib import admin
from contractors import models as contr_models


class CreateProhibitedAdminMixin:

    def has_add_permission(self, request):
        return False


class ContractorAdmin(CreateProhibitedAdminMixin, admin.ModelAdmin):
    pass


admin.site.register(contr_models.Contractor, ContractorAdmin)

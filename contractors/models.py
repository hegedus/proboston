from django.db import models


class Contractor(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255, null=True, blank=True)
    ico = models.CharField(max_length=8)

    def __str__(self):
        return "Contractor: {} - {} - {}".format(
            self.name,
            self.email,
            self.ico,
        )
